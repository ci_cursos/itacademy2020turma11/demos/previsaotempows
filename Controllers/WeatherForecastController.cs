﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace previsaotempows.Controllers
{
    [ApiController]
    [Route("api/previsao")]
    public class WeatherForecastController : ControllerBase
    {
        private static readonly string[] Summaries = new[]
        {
            "Freezing", "Bracing", "Chilly", "Cool", "Mild", "Warm", "Balmy", "Hot", "Sweltering", "Scorching"
        };

        private readonly ILogger<WeatherForecastController> _logger;
        //private readonly IConfiguration _configuration;

        public WeatherForecastController(ILogger<WeatherForecastController> logger)
        {
            _logger = logger;
        }

        [HttpGet]
        //https://localhost:5001/api/previsao
        public IEnumerable<WeatherForecast> Get()
        {
            _logger.LogInformation("Executando método GET");
            var rng = new Random();
            return Enumerable.Range(1, 5).Select(index => new WeatherForecast
            {
                Date = DateTime.Now.AddDays(index),
                TemperatureC = rng.Next(-20, 55),
                Summary = Summaries[rng.Next(Summaries.Length)]
            })
            .ToArray();
        }

        [HttpGet("versao")]
        //https://localhost:5001/api/previsao/versao
        public string GetVersao()
        {
            return "1.0.0";
        }

        [HttpGet("erro")]
        //https://localhost:5001/api/previsao/erro
        public void GetErro()
        {
            throw new Exception("Erro!");
        }

        [HttpGet("{cidade}")]
        //https://localhost:5001/api/previsao/portoalegre
        public IEnumerable<WeatherForecast> Get(string cidade)
        {
            _logger.LogInformation($"Executando método GET {cidade}");
            var rng = new Random();
            return Enumerable.Range(1, 5).Select(index => new WeatherForecast
            {
                Date = DateTime.Now.AddDays(index),
                TemperatureC = rng.Next(-20, 55),
                Summary = Summaries[rng.Next(Summaries.Length)]
            })
            .ToArray();
        }

        [HttpPost]
        //https://localhost:5001/api/previsao
        public IEnumerable<WeatherForecast> Post([FromBody] string cidade)
        {
            _logger.LogInformation($"Executando método POST {cidade}");
            var rng = new Random();
            return Enumerable.Range(1, 5).Select(index => new WeatherForecast
            {
                Date = DateTime.Now.AddDays(index),
                TemperatureC = rng.Next(-20, 55),
                Summary = Summaries[rng.Next(Summaries.Length)]
            })
            .ToArray();
        }
    }
}
